"use strict";

const winston = require("winston");
const pretty_f = require("pretty-format");
const stream = require("stream");
const chalk = require("chalk");
const
{
    SPLAT
} = require("triple-beam");

const levels = {
    critical:
    {
        index: 0,
        title: "Critical",
        color: "#c0392b"
    },
    error:
    {
        index: 1,
        title: "Error",
        color: "#c0392b"
    },
    warning:
    {
        index: 2,
        title: "Warning",
        color: "#e67e22"
    },
    notice:
    {
        index: 3,
        title: "Notice",
        color: "#27ae60"
    },
    info:
    {
        index: 4,
        title: "Info",
        color: "#27ae60"
    },
    debug:
    {
        index: 5,
        title: "Debug",
        color: "#8e44ad"
    }
}

function to_string(el, color)
{
    if (el instanceof Error)
        return ` ${el.stack}`;
    else if (typeof el !== "string")
        return ` ${pretty_f(el, {
        indent: 4,
        highlight: color
    })}`;
    return ` ${el}`;
}

function logger_format(info, color)
{
    let base = "\r";
    let res;

    if (color)
        base = `[${chalk.hex(levels[info.level].color)(info.level.toUpperCase())}] (${info.timestamp}): `;
    else
        base = `[${info.level.toUpperCase()}] (${info.timestamp}): `;
    if (info instanceof Error)
        res = ` ${info.stack}`;
    else
        res = to_string(info.message, true)
        .substring(1);
    if (Array.isArray(info[SPLAT]))
        info[SPLAT].forEach((el) => res += to_string(el, color));
    else if (info[SPLAT] !== undefined)
        res += to_string(info[SPLAT], color);
    return (base + res);
}

class ConsoleStream extends stream.Writable
{
    _write(chunk, encoding, cb)
    {
        console.log(logger_format(chunk, process.stdout.isTTY));
        cb();
    }
}

const console_stream = new ConsoleStream(
{
    objectMode: true
});

var transport = [
    new winston.transports.Stream(
    {
        stream: console_stream
    })
];

function get_logger(lvls)
{
    return winston.createLogger(
    {
        levels: lvls || levels,
        format: winston.format.combine(
            winston.format.splat(),
            winston.format.timestamp(),
        ),
        transports: transport
    });
}

module.exports = {
    get_logger
};