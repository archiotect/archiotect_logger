"use strict";

const Joi = require("joi");
const _ = require("lodash");
const Promise = require("bluebird");
const logger = require("./logger");

const config_schema = Joi.object()
    .keys(
    {
        levels: Joi.array()
            .items()
            .min(1)
            .items(Joi.object()
                .keys(
                {
                    index: Joi.number()
                        .min(0)
                        .required(),
                    title: Joi.string()
                        .required(),
                    color: Joi.string()
                        .required()
                        .regex(/^#[a-z0-9]{6}$/)
                })
                .required())
    });

/**
 * Load this plugins
 * 
 * @param {Object} conf The configuration object
 * @param {Object} archiotect The Archiotect object
 * @returns {Promise} resolve with config if successful otherwise reject with error
 */
function load(conf, archiotect)
{
    var l;

    l = logger.get_logger(conf);
    if (_.isFunction(logger.info))
        archiotect.on(/.*/, (...args) => logger.info(...args))
    return Promise.resolve(
    {
        logger: l
    });
}

/**
 * Function called before and after each plugin loading
 *
 * @returns {Promise} Return a falsy value if the plugin should continue to load otherwise the plugin will stop loading,
 * and the return value used as error message
 */
function handle()
{
    return Promise.resolve(false);
}

module.exports = {
    name: "Archiotect_logger",
    version: "0.1.0",
    need: ["Archiotect_config"],
    core: true,
    load,
    handle,
    config_schema
}